package gr.dkarao.gitlearning;

public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello World");
		System.out.println("This commit is for the first branch");
		System.out.println("Hello from user2!");
		System.out.println("Sum = " + new Calculator().add(1, 2));
		System.out.println("Difference = " + new Calculator().subtract(1, 2));
		System.out.println("This is a change from user2");
		System.out.println("This is a change from user1");
		System.out.println("Product = " + new Calculator().multiply(1, 2));
		System.out.println("Division = " + new Calculator().divide(1, 2));
		System.out.println("Remainder = " + new Calculator().modulus(1, 2));
		System.out.println("Max = " + new Calculator().max(1, 2));
	}

}
