package gr.dkarao.gitlearning;

import java.math.BigDecimal;

public class Calculator {
	
	public int add(int a, int b) {
		return a+b;
	}
	
	public int subtract(int a, int b) {
		return a-b;
	}
	
	public BigDecimal divide(int a, int b) {
		return new BigDecimal(a).divide(new BigDecimal(b));
	}
	
	public int multiply(int a, int b) {
		return a*b;
	}
	
	public BigDecimal modulus(int a, int b) {
		return new BigDecimal(a).remainder(new BigDecimal(b));
	}
	
	public BigDecimal max(int a, int b) {
		return new BigDecimal(a).max(new BigDecimal(b));
	}
}
